import React from 'react';
import '../App.css';

const Posts = ({ posts, loading }) => {
  if (loading) {
    return <h3>posts loading ...</h3>;
  }

  return (
    <ul>
      {posts.map(post => (
        <li key={post.id} className='listItems'>
          {post.title}
        </li>
      ))}
    </ul>
  );
};

export default Posts;
