import React from 'react';

const Pages = ({ postSrt, totalPosts, paginate }) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postSrt); i++) {
    pageNumbers.push(i);
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <div className='pages'>
              {pageNumbers.map(number => (
                <a onClick={() => paginate(number)} href='!#' className='page' key={number}>
                  {number}
                </a>
              ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Pages;
