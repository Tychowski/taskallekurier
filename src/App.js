import React, { useState, useEffect } from 'react';
import Posts from './components/Posts';
import Pages from './components/Pages';
import axios from 'axios';
import './App.css';
import logo from './logo.png';

const App = () => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postSrt] = useState(15);

  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      const res = await axios.get('https://jsonplaceholder.typicode.com/posts');
      setPosts(res.data);
      setLoading(false);
    };

    fetchPosts();
  }, []);

  const lastIndex = currentPage * postSrt;
  const firstIndex = lastIndex - postSrt;
  const currentPosts = posts.slice(firstIndex, lastIndex);

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <div className='container'>
      <div className="row">
        <div className="col">
          <h1 className='title'>
            Zadanie testowe 
            <img src={logo} width="180" alt="logo" />
          </h1>
          <h3 className="subTitle">API request</h3>

          <Posts posts={currentPosts} loading={loading} />

          <div className="Pages">
            <Pages
              postSrt={postSrt}
              totalPosts={posts.length}
              paginate={paginate}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
